# README

## À propos de Devoxx FR 2014

Devoxx FR is a conference for developers.

**Thème :** « Comment devenir développeur … et le rester »

## Comment formatter les documents

Les différentes pages ont été écrite en utilisant le _markup language_ « Markdown ».

Il suffit d'utiliser le formateur de son choix afin de convertir les documents en PDF, html, LaTeX, …

Ce projet propose l'utilisation de `gradle` (pour changer un peu de `Maven`) et du plugin `markdown-gradle-plugin` pour
le formattage. Il suffit d'exécuter la commande suivante à la racine du projet :

```sh
gradlew.bat :devoxx-notes:markdownToHtml
```